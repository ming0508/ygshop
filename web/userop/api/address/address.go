package address

import (
	"context"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
	"strconv"
	"web/userop/api"
	"web/userop/forms"
	"web/userop/global"
	"web/userop/proto"
)

// 获取用户的收获地址列表
func GetAddressList(ctx *gin.Context) {
	var userId int32
	if val, ok := ctx.Get("userId"); ok {
		userId = val.(int32)
	} else {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"msg": "请重新登录",
		})
		return
	}

	rsp, err := global.UseropSrvClient.GetAddressList(context.Background(), &proto.AddressRequest{
		UserId: userId,
	})
	if err != nil {
		zap.S().Error("获取用户留言列表失败：", err)
		api.HandleGrpcErrorToHttp(err, ctx)
		return
	}

	var m []gin.H
	for _, v := range rsp.Data {
		m = append(m, gin.H{
			"id":            v.Id,
			"user_id":       v.UserId,
			"province":      v.Province,
			"city":          v.City,
			"district":      v.District,
			"address":       v.Address,
			"signer_name":   v.SignerName,
			"signer_mobile": v.SignerMobile,
		})
	}
	ctx.JSON(http.StatusOK, m)
}

// 用户的添加收获地址
func AddAddress(ctx *gin.Context) {
	f := new(forms.AddressForm)
	if err := ctx.ShouldBind(&f); err != nil {
		api.HandleValidatorError(ctx, err)
		return
	}

	var userId int32
	if val, ok := ctx.Get("userId"); ok {
		userId = val.(int32)
	} else {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"msg": "请重新登录",
		})
		return
	}

	res, err := global.UseropSrvClient.CreateAddress(context.Background(), &proto.AddressRequest{
		UserId:       userId,
		Province:     f.Province,
		City:         f.City,
		District:     f.District,
		Address:      f.Address,
		SignerName:   f.SignerName,
		SignerMobile: f.SignerMobile,
	})
	if err != nil {
		api.HandleGrpcErrorToHttp(err, ctx)
		zap.S().Error("添加留言信息失败：", err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// 用户更新收获地址
func UpdateAddress(ctx *gin.Context) {
	f := new(forms.AddressForm)
	if err := ctx.ShouldBind(&f); err != nil {
		api.HandleValidatorError(ctx, err)
		return
	}

	idStr := ctx.Param("id")
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		zap.S().Error("strconv.Atoi：", err)
		ctx.JSON(http.StatusNotFound, gin.H{
			"msg": "解析参数出错，请检查收货地址的id参数",
		})
		return
	}

	if _, err := global.UseropSrvClient.UpdateAddress(context.Background(), &proto.AddressRequest{
		Id:           int32(idInt),
		Province:     f.Province,
		City:         f.City,
		District:     f.District,
		Address:      f.Address,
		SignerName:   f.SignerName,
		SignerMobile: f.SignerMobile,
	}); err != nil {
		api.HandleGrpcErrorToHttp(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"msg": "更新成功",
	})
}

// 用户删除收获地址
func DeleteAddress(ctx *gin.Context) {
	idStr := ctx.Param("id")
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		zap.S().Error("strconv.Atoi：string转int类型发生错误")
		ctx.JSON(http.StatusNotFound, gin.H{
			"msg": "解析参数出错，请检查收货地址主键id参数",
		})
		return
	}

	_, err = global.UseropSrvClient.DeleteAddress(context.Background(), &proto.AddressRequest{
		Id: int32(idInt),
	})
	if err != nil {
		api.HandleGrpcErrorToHttp(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"msg": "删除成功",
	})
}
