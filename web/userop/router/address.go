package router

import (
	"github.com/gin-gonic/gin"
	"web/userop/api/address"
	"web/userop/middlewares"
)

func InitAddressRouter(router *gin.RouterGroup) {
	orderRouter := router.Group("address").Use(middlewares.JWTAuth())
	{
		orderRouter.GET("", address.GetAddressList)
		orderRouter.POST("", address.AddAddress)
		orderRouter.PUT(":id", address.UpdateAddress)
		orderRouter.DELETE(":id", address.DeleteAddress)
	}
}
