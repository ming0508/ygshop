package router

import (
	"github.com/gin-gonic/gin"
	"web/userop/api/userfav"
	"web/userop/middlewares"
)

func InitUserfavRouter(router *gin.RouterGroup) {
	orderRouter := router.Group("userfav").Use(middlewares.JWTAuth())
	{
		orderRouter.POST("", userfav.AddUserfav)
		orderRouter.DELETE("", userfav.DeleteUserfav)
	}
}
