package router

import (
	"github.com/gin-gonic/gin"
	"web/userop/api/message"
	"web/userop/middlewares"
)

func InitMessageRouter(router *gin.RouterGroup) {
	orderRouter := router.Group("message").Use(middlewares.JWTAuth())
	{
		orderRouter.GET("", message.GetMessageList)
		orderRouter.POST("", message.AddMessage)
	}
}
