package initialize

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"web/userop/router"
)

func Routers() *gin.Engine {
	r := gin.Default()

	// 跨域设置
	config := cors.DefaultConfig()
	// 允许所有域名
	config.AllowAllOrigins = true
	// 允许请求的方法
	config.AllowMethods = []string{"GET", "POST", "OPTIONS", "DELETE", "PATH", "PUT"}
	// 允许的Header
	config.AllowHeaders = []string{"Content-Type", "AccessToken", "X-CSRF-Token", "Authorization", "Token", "x-token"}
	r.Use(cors.New(config))

	apiGroup := r.Group("/up/v1")

	router.InitMessageRouter(apiGroup)
	router.InitAddressRouter(apiGroup)
	router.InitUserfavRouter(apiGroup)

	router.InitHealthRouter(r)
	return r
}
