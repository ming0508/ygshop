package forms

// 密码登录的表单
type PasswordLoginForm struct {
	Mobile    string `form:"mobile" json:"mobile" binding:"required,mobile"`
	Password  string `form:"password" json:"password" binding:"required,min=3,max=20"`
	Captcha   string `form:"captcha" json:"captcha" binding:"required,min=5,max=5"`
	CaptchaId string `form:"captcha_id" json:"captcha_id" binding:"required"`
}

// 用户注册的表单
type RegisterForm struct {
	Mobile   string `form:"mobile" json:"mobile" binding:"required,mobile"`
	SmsCode  string `form:"sms_code" json:"sms_code" binding:"required,min=6,max=6"` // 短信验证码
	Password string `form:"password" json:"password" binding:"required,min=3,max=20"`
}
