package forms

type SendSmsForm struct {
	Mobile string `form:"mobile" json:"mobile" binding:"required,mobile"`
	// 短信发送的业务类型：用户注册、找回密码、动态验证码登录
	// 1表示用户注册，2表示动态验证码登录
	Type int `form:"type" json:"type" binding:"required,oneof=1 2"`
}
