package config

// 读取yaml配置文件后映射的结构体

type UserSrvConfig struct {
	Name string `mapstructure:"name" json:"name"`
}

type JWTConfig struct {
	SigningKey string `mapstructure:"key" json:"key"`
}

// 对接阿里云短信服务相关配置
type AliSmsConfig struct {
	ApiKey       string `mapstructure:"key" json:"key"`       // accessKeyId
	ApiSecret    string `mapstructure:"secret" json:"secret"` // accessKeySecret
	SignName     string `mapstructure:"sign_name" json:"sign_name"`
	TemplateCode string `mapstructure:"template_code" json:"template_code"`
}

type RedisConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
}

type ConsulConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
}

type ServerConfig struct {
	Name        string        `mapstructure:"name" json:"name"`
	Host        string        `mapstructure:"host" json:"host"`
	Port        int           `mapstructure:"port" json:"port"`
	Tags        []string      `mapstructure:"tags" json:"tags"`
	UserSrvInfo UserSrvConfig `mapstructure:"user_srv" json:"user_srv"`
	JWTInfo     JWTConfig     `mapstructure:"jwt" json:"jwt"`
	AliSmsInfo  AliSmsConfig  `mapstructure:"sms" json:"sms"`
	RedisInfo   RedisConfig   `mapstructure:"redis" json:"redis"`
	ConsulInfo  ConsulConfig  `mapstructure:"consul" json:"consul"`
}

type NacosConfig struct {
	IpAddr    string `mapstructure:"ip_addr"`
	Port      uint64 `mapstructure:"port"`
	UserName  string `mapstructure:"username"`
	Password  string `mapstructure:"password"`
	Namespace string `mapstructure:"namespace_id"`
	DataId    string `mapstructure:"data_id"`
	Group     string `mapstructure:"group"`
}
