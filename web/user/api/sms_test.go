package api

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/spf13/viper"
	"strconv"
	"testing"
	"time"
	"web/user/global"
)

// 测试生成短信验证码
func TestGenerateSmsCode(t *testing.T) {
	for i := 0; i < 1e6; i++ {
		if len(GenerateSmsCode(6)) != 6 {
			t.Error("短信验证码长度出错")
			break
		}
	}
	t.Log("短信验证码长度正常")
}

// 获取系统的环境变量
func GetEnvInfo(env string) bool {
	viper.AutomaticEnv()
	return viper.GetBool(env)
}

// 测试保存短信验证码到redis
func TestSaveSmsCodeToRedis(t *testing.T) {
	debug := GetEnvInfo("YGSHOP_DEBUG")
	configFilePrefix := "config"
	configFileName := fmt.Sprintf("./%s-pro.yaml", configFilePrefix)
	if debug {
		configFileName = fmt.Sprintf("./%s-debug.yaml", configFilePrefix)
	}

	v := viper.New()
	// 设置配置文件路径
	v.SetConfigFile(configFileName)
	// 将配置文件的内容读进内存
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}
	// 将内存中的配置文件信息解析到全局变量中
	if err := v.Unmarshal(global.ServerConfig); err != nil {
		panic(err)
	}

	// 将手机验证码保存到redis
	redisAddr := global.ServerConfig.RedisInfo.Host + ":" + strconv.Itoa(global.ServerConfig.RedisInfo.Port)
	t.Log(redisAddr)
	rdb := redis.NewClient(&redis.Options{
		Addr: redisAddr,
	})
	// 最后一个参数是记录的过期时间
	s := rdb.Set(context.Background(), "xxxxx", 666666, time.Minute*5)
	t.Log(s.Err())
}
