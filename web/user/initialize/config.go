package initialize

import (
	"encoding/json"
	"fmt"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"web/user/global"
)

// 获取系统的环境变量
func GetEnvInfo(env string) bool {
	viper.AutomaticEnv()
	return viper.GetBool(env)
}

func InnitConfig() {
	debug := GetEnvInfo("YGSHOP_DEBUG")
	configFilePrefix := "config-nacos"
	configFileName := fmt.Sprintf("./%s-pro.yaml", configFilePrefix)
	if debug {
		configFileName = fmt.Sprintf("./%s-dev.yaml", configFilePrefix)
	}

	v := viper.New()
	// 设置配置文件路径
	v.SetConfigFile(configFileName)
	// 将配置文件的内容读进内存
	if err := v.ReadInConfig(); err != nil {
		zap.S().Fatal("将本地配置文件的内容读进内存失败: ", err)
	}

	// 将内存中的配置文件信息解析到全局变量中
	if err := v.Unmarshal(global.NacosConfig); err != nil {
		zap.S().Fatal("解析nacos配置失败: ", err)
	}
	zap.S().Infof("nacos连接信息: %#v", global.NacosConfig)

	// 从nacos服务中读取配置信息
	// nacos服务端信息
	serverConfigs := []constant.ServerConfig{
		{
			IpAddr: global.NacosConfig.IpAddr,
			Port:   global.NacosConfig.Port,
		},
	}

	// 客户端配置
	clientConfig := constant.ClientConfig{
		NamespaceId:         global.NacosConfig.Namespace,
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./tmp/nacos/log",
		CacheDir:            "./tmp/nacos/cache",
		RotateTime:          "1h",
		MaxAge:              3,
		LogLevel:            "debug",
		Username:            global.NacosConfig.UserName,
		Password:            global.NacosConfig.Password,
	}

	// 创建客户端
	configClient, err := clients.CreateConfigClient(map[string]interface{}{
		"serverConfigs": serverConfigs,
		"clientConfig":  clientConfig,
	})
	if err != nil {
		zap.S().Fatal("创建nacos客户端失败: ", err)
	}

	// 通过客户端获取nacos中保存的配置信息
	content, err := configClient.GetConfig(vo.ConfigParam{
		DataId: global.NacosConfig.DataId,
		Group:  global.NacosConfig.Group,
	})
	if err != nil {
		zap.S().Fatal("通过客户端获取nacos中保存的配置信息失败: ", err)
	}

	// 解析配置信息
	json.Unmarshal([]byte(content), global.ServerConfig)

	// 监听配置变化
	err = configClient.ListenConfig(vo.ConfigParam{
		DataId: global.NacosConfig.DataId,
		Group:  global.NacosConfig.Group,
		OnChange: func(namespace, group, dataId, data string) {
			// data就是对应配置文件中的内容
			// 配置信息发生变化，重新解析
			json.Unmarshal([]byte(data), global.ServerConfig)
			zap.S().Infof("配置已被更改，最新信息: %#v", global.ServerConfig)
		},
	})
	if err != nil {
		zap.S().Fatal("监听配置信息失败: ", err)
	}

	zap.S().Infof("配置中心信息: %#v", global.ServerConfig)
}
