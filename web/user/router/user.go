package router

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"web/user/api"
	"web/user/middlewares"
)

func InitUserRouter(router *gin.RouterGroup) {
	userRouter := router.Group("user")
	zap.L().Info("配置用户相关的路由")
	{
		userRouter.GET("list", middlewares.JWTAuth(), middlewares.IsAdminAuth(), api.GetUserList)
		userRouter.POST("pwd_login", api.PassWordLogin)
		// 请求注册
		userRouter.POST("register", api.Register)
	}
}
