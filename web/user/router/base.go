package router

import (
	"github.com/gin-gonic/gin"
	"web/user/api"
)

func InitBaseRouter(router *gin.RouterGroup) {
	baseRouter := router.Group("base")
	{
		// 请求获取验证码
		baseRouter.GET("captcha", api.GetCaptcha)
		// 请求获取短信验证码
		baseRouter.POST("send_sms", api.SendSms)
	}
}
