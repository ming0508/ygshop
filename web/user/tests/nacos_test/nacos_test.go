package nacos_test_test

import (
	"encoding/json"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"testing"
	"time"
	"web/user/config"
)

func TestNacos(t *testing.T) {
	// nacos服务端信息
	serverConfigs := []constant.ServerConfig{
		{
			IpAddr: "127.0.0.1",
			Port:   8848,
		},
	}

	// 客户端配置
	clientConfig := constant.ClientConfig{
		NamespaceId:         "0a6a57de-b343-4c4c-8a78-7b031f265881", // 如果需要支持多namespace，我们可以场景多个client,它们有不同的NamespaceId。当namespace是public时，此处填空字符串。
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "/tmp/nacos/log",
		CacheDir:            "/tmp/nacos/cache",
		RotateTime:          "1h",
		MaxAge:              3,
		LogLevel:            "debug",
		Username:            "admin",
		Password:            "admin",
	}

	// 创建客户端
	configClient, err := clients.CreateConfigClient(map[string]interface{}{
		"serverConfigs": serverConfigs,
		"clientConfig":  clientConfig,
	})
	if err != nil {
		t.Fatal(err)
	}

	// 通过客户端获取nacos中保存的配置信息
	content, err := configClient.GetConfig(vo.ConfigParam{
		DataId: "user-web.json",
		Group:  "dev",
	})
	if err != nil {
		t.Fatal(err)
	}

	serverConfig := new(config.ServerConfig)
	json.Unmarshal([]byte(content), serverConfig)

	//监听配置变化
	err = configClient.ListenConfig(vo.ConfigParam{
		DataId: "user-web.json",
		Group:  "dev",
		OnChange: func(namespace, group, dataId, data string) {
			// data就是对应配置文件中的内容
			//t.Log("group:" + group + ", dataId:" + dataId + ", data:\n" + data)
			json.Unmarshal([]byte(data), serverConfig)
		},
	})
	if err != nil {
		t.Fatal(err)
	}

	for {
		t.Logf("%#v", serverConfig)
		time.Sleep(time.Second * 5)
	}
}
