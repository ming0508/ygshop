package consul_test

import (
	"fmt"
	"github.com/hashicorp/consul/api"
	"testing"
)

// 参数说明
// address：待注册的服务的ip
// name：服务名称
// id：服务id
// port：服务端口
// tags：服务标签
func Register(address, name, id string, port int, tags []string) error {
	cfg := api.DefaultConfig()
	// 设置consul服务运行所在的ip和端口
	//cfg.Address = "10.58.85.159:8500"
	//cfg.Address的ip可以是127.0.0.1

	client, err := api.NewClient(cfg)
	if err != nil {
		panic(err)
	}

	// 生成健康检查对象
	check := &api.AgentServiceCheck{
		// 这里的ip不可以是127.0.0.1
		HTTP:                           "http://10.58.85.159:8021/health", // user-web服务的运行地址
		Timeout:                        "5s",                              // 超过此时间说明服务状态不健康
		Interval:                       "5s",                              // 每5s检查一次
		DeregisterCriticalServiceAfter: "30s",                             // 失败多久后注销服务
	}

	// 生成注册对象
	registration := &api.AgentServiceRegistration{
		Name:    name,
		ID:      id,
		Address: address,
		Port:    port,
		Tags:    tags,
		Check:   check,
	}

	// 注册服务
	return client.Agent().ServiceRegister(registration)
}

// 服务发现——获取所有已注册的服务
func AllServices() {
	cfg := api.DefaultConfig()
	client, err := api.NewClient(cfg)
	if err != nil {
		panic(err)
	}
	// 这里获取的服务是包括"不健康"服务的
	data, err := client.Agent().Services()
	if err != nil {
		panic(err)
	}
	for k, v := range data {
		fmt.Println(k, "-----", v)
	}
}

// 服务发现——获取符合条件的服务
func FilterServices() {
	cfg := api.DefaultConfig()
	client, err := api.NewClient(cfg)
	if err != nil {
		panic(err)
	}
	// 01.按服务的name进行过滤，结果不唯一。这里获取的服务是包括"不健康"服务的
	// expected: "!=", "==", "contains", "in", "is", "matches", "not" or [ \t\r\n]) [recovered]
	//data,err:=client.Agent().ServicesWithFilter(`Service=="user-web"`)
	//for k,v:=range data{
	//	fmt.Println(k,"-----",v)
	//}

	// 02.直接通过服务ID过滤，结果唯一
	//service, _, err := client.Agent().Service("user-web01", nil)
	//if err!=nil{
	//	panic(err)
	//}
	//fmt.Println(service)

	// 03.根据服务健康状态和name、tag进行过滤，结果不唯一
	// 这个方式应该更常用一些。
	serviceHealthy, _, err := client.Health().Service("user-web", "", true, nil)
	if err == nil {
		fmt.Println(serviceHealthy[0].Service)
	}

}

func TestConsul(t *testing.T) {
	// 如果多个服务的 name 一致而 id 不一致，这些服务是会被放置到同一 name 分组的。
	// 这里的ip不可以是127.0.0.1
	//if err := Register("10.58.85.159", "user-web", "user-web02", 8021, []string{"ygshop", "web"}); err != nil {
	//	panic(err)
	//}

	//AllServices()
	FilterServices()
}
