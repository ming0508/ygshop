package grpclb_test

import (
	"context"
	_ "github.com/mbobakov/grpc-consul-resolver"
	"google.golang.org/grpc"
	"testing"
	"web/user/proto"
)

// 测试：使用负载均衡算法来从consul中发现服务
func TestGrpcConsulResolver(t *testing.T) {
	conn, err := grpc.Dial(
		"consul://127.0.0.1:8500/user-srv?healthy=true&wait=14s&tag=yg_shop",
		grpc.WithInsecure(),
		// grpc目前似乎只支持轮询的负载均衡算法
		grpc.WithDefaultServiceConfig(`{"loadBalancingPolicy": "round_robin"}`),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()

	userSrvClient := proto.NewUserClient(conn)
	for i := 0; i < 10; i++ {
		rsp, err := userSrvClient.GetUserList(context.Background(), &proto.PageInfo{
			Pn:    1,
			PSize: 2,
		})
		if err != nil {
			t.Fatal(err)
		}
		for i, v := range rsp.Data {
			t.Log(i, "------", v)
		}
	}
}
