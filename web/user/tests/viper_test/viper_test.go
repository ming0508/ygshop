package viper_test

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"testing"
	"time"
)

// mapstructure是viper的标签
type ServerConfig struct {
	ServiveName string `mapstructure:"name"`
	Port        int    `mapstructure:"port"`
}

type MysqlConfig struct {
	Host string `mapstructure:"host"`
	Port int    `mapstructure:"port"`
}

type ServerConfig02 struct {
	ServiveName string      `mapstructure:"name"`
	MysqlInfo   MysqlConfig `mapstructure:"mysql"`
}

func TestViper(t *testing.T) {
	v := viper.New()
	// 设置要读取的配置文件的路径
	v.SetConfigFile("./config.yaml")
	// 将配置文件中的内容加载到viper对象中
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}

	// 新建一个结构体对象，准备将配置文件的内容读进这个对象
	serverConfig := new(ServerConfig)
	// 将v中加载好的配置文件信息映射到一个结构体对象上
	if err := v.Unmarshal(&serverConfig); err != nil {
		panic(err)
	}

	t.Logf("%#v\n", serverConfig)

	// 直接读取，这里没有映射到结构体
	t.Log(v.Get("name"))
}

// 将配置文件信息读到嵌套结构体
func TestViper02(t *testing.T) {
	v := viper.New()
	// 设置要读取的配置文件的路径
	v.SetConfigFile("./config-debug.yaml")
	// 将配置文件中的内容加载到viper对象中
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}

	// 新建一个结构体对象，准备将配置文件的内容读进这个对象
	serverConfig := new(ServerConfig02)
	// 将v中加载好的配置文件信息映射到一个结构体对象上
	if err := v.Unmarshal(&serverConfig); err != nil {
		panic(err)
	}

	t.Logf("%#v\n", serverConfig)
}

// 获取系统的环境变量
func GetEnvInfo(env string) bool {
	viper.AutomaticEnv()
	return viper.GetBool(env)
}

// 使用viper设置开发环境与生产环境的隔离
func TestViper03(t *testing.T) {
	debug := GetEnvInfo("YGSHOP_DEBUG")
	//t.Log(debug)
	configFilePrefix := "config"
	configFileName := fmt.Sprintf("./%s-pro.yaml", configFilePrefix)
	if debug {
		configFileName = fmt.Sprintf("./%s-debug.yaml", configFilePrefix)
	}

	v := viper.New()
	v.SetConfigFile(configFileName)
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}
	serverConfig := new(ServerConfig02)
	if err := v.Unmarshal(serverConfig); err != nil {
		panic(err)
	}

	t.Logf("%#v\n", serverConfig)

	// 使用viper动态监控配置文件的变化
	// 启动后，修改config-debug.yaml并保存即可看到终端输出的变化
	v.WatchConfig()
	v.OnConfigChange(func(in fsnotify.Event) {
		t.Log("config file changed:", in.Name)
		v.ReadInConfig()
		v.Unmarshal(serverConfig)
		t.Logf("%#v\n", serverConfig)
	})

	time.Sleep(time.Second * 300)
}
