package zap_test

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"testing"
	"time"
)

// 测试终端输出日志
func TestZapToTerminal(t *testing.T) {
	// 生产环境
	logger, _ := zap.NewProduction()
	// 开发环境
	//logger, _ := zap.NewDevelopment()

	// 刷新缓存
	defer logger.Sync() // flushes buffer, if any
	url := "https://www.baidu.com"

	// 语法糖，简化使用
	//sugar := logger.Sugar()
	//// 末尾是'w'就表示日志信息的格式为：提示信息+关键字段信息(键值对)
	//sugar.Infow("failed to fetch URL",
	//	// Structured context as loosely typed key-value pairs.
	//	"url", url,
	//	"attempt", 3,
	//)
	//sugar.Infof("Failed to fetch URL: %s", url)

	// 如果要追求极致性能，可以弃用语法糖
	logger.Info("failed to fetch URL",
		zap.String("url", url),
		zap.Int("attempt", 3))
}

func NewLogger() (*zap.Logger, error) {
	cfg := zap.NewProductionConfig()
	// 设置日志颜色
	cfg.EncoderConfig.EncodeLevel = zapcore.LowercaseColorLevelEncoder
	cfg.OutputPaths = []string{
		"./myproject.log",
		// 以下两种输出选择的差异就是终端显示的颜色不一样（单元测试的输出里没有颜色）
		"stderr",
		//"stdout",
	}
	return cfg.Build()
}

// 测试文件输出日志
func TestZapToFile(t *testing.T) {
	logger, err := NewLogger()
	if err != nil {
		panic("初始化logger失败" + err.Error())
	}
	su := logger
	defer su.Sync()
	url := "https://www.baidu.com"
	su.Info("failed to fetch URL",
		zap.String("url", url),
		zap.Int("attempt", 3),
		zap.Duration("backoff", time.Second),
	)
}
