package router

import (
	"github.com/gin-gonic/gin"
	"web/goods/api/brands"
	"web/goods/middlewares"
)

func InitBrandsRouter(router *gin.RouterGroup) {
	categoryRouter := router.Group("brands")
	{
		// 除了get请求，其他请求都需要登录认证和管理员权限认证
		categoryRouter.GET("", brands.GetBrandsList)
		categoryRouter.POST("", middlewares.JWTAuth(), middlewares.IsAdminAuth(), brands.CreateBrand)
		categoryRouter.PUT(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), brands.UpdateBrand)
		categoryRouter.DELETE(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), brands.DeleteBrand)
	}
}
