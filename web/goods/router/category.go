package router

import (
	"github.com/gin-gonic/gin"
	"web/goods/api/category"
	"web/goods/middlewares"
)

func InitCategoryRouter(router *gin.RouterGroup) {
	categoryRouter := router.Group("categorys")
	{
		// 除了get请求，其他请求都需要登录认证和管理员权限认证
		categoryRouter.POST("", middlewares.JWTAuth(), middlewares.IsAdminAuth(), category.CreateCategory)
		categoryRouter.GET("", category.GetCategories)
		categoryRouter.PUT(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), category.UpdateCategory)
		categoryRouter.DELETE(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), category.DeleteCategory)
		categoryRouter.GET(":id", category.GetCategoryInfo)
	}
}
