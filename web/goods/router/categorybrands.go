package router

import (
	"github.com/gin-gonic/gin"
	"web/goods/api/categorybrands"
	"web/goods/middlewares"
)

func InitCategorybrandsRouter(router *gin.RouterGroup) {
	categoryRouter := router.Group("categorybrands")
	{
		// 除了get请求，其他请求都需要登录认证和管理员权限认证
		categoryRouter.GET(":id", categorybrands.GetCategoryBrands)
		categoryRouter.GET("", categorybrands.GetCategoryBrandList)
		categoryRouter.POST("", middlewares.JWTAuth(), middlewares.IsAdminAuth(), categorybrands.AddCategorybrand)
		categoryRouter.PUT(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), categorybrands.UpdateCategoBrand)
		categoryRouter.DELETE(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), categorybrands.DeleteCategorybrand)
	}
}
