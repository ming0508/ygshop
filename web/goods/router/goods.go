package router

import (
	"github.com/gin-gonic/gin"
	"web/goods/api/goods"
	"web/goods/middlewares"
)

func InitGoodsRouter(router *gin.RouterGroup) {
	goodsRouter := router.Group("goods")
	{
		// 获取商品列表时，限流：每6秒最多请求3次
		goodsRouter.GET("", middlewares.FlowControl(), goods.GetGoodsList)
		goodsRouter.GET(":id", goods.GetGoodInfo)
		goodsRouter.GET(":id/stocks", goods.GetGoodsStocks)
		// 除了get请求，其他请求都需要登录认证和管理员权限认证
		goodsRouter.POST("", middlewares.JWTAuth(), middlewares.IsAdminAuth(), goods.CreateGood)
		goodsRouter.DELETE(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), goods.DeleteGood)
		goodsRouter.PUT(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), goods.UpdateGoodInfo)
		goodsRouter.PATCH(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), goods.UpdateGoodStatus)
	}
}
