package router

import (
	"github.com/gin-gonic/gin"
	"web/goods/api/banner"
	"web/goods/middlewares"
)

func InitBannerRouter(router *gin.RouterGroup) {
	categoryRouter := router.Group("banners")
	{
		// 除了get请求，其他请求都需要登录认证和管理员权限认证
		categoryRouter.GET("", banner.GetBannerList)
		categoryRouter.POST("", middlewares.JWTAuth(), middlewares.IsAdminAuth(), banner.CreateBanner)
		categoryRouter.PUT(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), banner.UpdateBanner)
		categoryRouter.DELETE(":id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), banner.DeleteBanner)
	}
}
