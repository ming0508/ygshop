package config

// 读取yaml配置文件后映射的结构体

type GoodsSrvConfig struct {
	Name string `mapstructure:"name" json:"name"`
}

type UserSrvConfig struct {
	Name string `mapstructure:"name" json:"name"`
}

type InventorySrvConfig struct {
	Name string `mapstructure:"name" json:"name"`
}

type JWTConfig struct {
	SigningKey string `mapstructure:"key" json:"key"`
}

type ConsulConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
}

type ServerConfig struct {
	Name             string             `mapstructure:"name" json:"name"` // 服务注册名称
	Host             string             `mapstructure:"host" json:"host"` // 服务运行所在ip、服务注册ip
	Port             int                `mapstructure:"port" json:"port"` // 服务运行所在端口、服务注册端口
	Tags             []string           `mapstructure:"tags" json:"tags"`
	GoodsSrvInfo     GoodsSrvConfig     `mapstructure:"goods_srv" json:"goods_srv"`
	UserSrvInfo      UserSrvConfig      `mapstructure:"user_srv" json:"user_srv"`
	InventorySrvInfo InventorySrvConfig `mapstructure:"inventory_srv" json:"inventory_srv"`
	JWTInfo          JWTConfig          `mapstructure:"jwt" json:"jwt"`
	ConsulInfo       ConsulConfig       `mapstructure:"consul" json:"consul"`
}

type NacosConfig struct {
	IpAddr    string `mapstructure:"ip_addr"`
	Port      uint64 `mapstructure:"port"`
	UserName  string `mapstructure:"username"`
	Password  string `mapstructure:"password"`
	Namespace string `mapstructure:"namespace_id"`
	DataId    string `mapstructure:"data_id"`
	Group     string `mapstructure:"group"`
}
