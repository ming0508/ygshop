package initialize

import (
	sentinel "github.com/alibaba/sentinel-golang/api"
	"github.com/alibaba/sentinel-golang/core/flow"
	"go.uber.org/zap"
)

func InitSentinel() {
	// 初始化sentinel
	err := sentinel.InitDefault()
	if err != nil {
		zap.S().Error("初始化sentinel异常：", err)
		return
	}

	// 配置限流规则
	_, err = flow.LoadRules([]*flow.Rule{
		// 各个字段的具体含义可参考：https://sentinelguard.io/zh-cn/docs/golang/flow-control.html
		// qps上限为10
		{
			Resource:               "get goods list",
			TokenCalculateStrategy: flow.Direct,
			// 超过阈值直接拒绝
			ControlBehavior: flow.Reject,
			// 流控阈值，6秒最多请求3次【测试用】
			Threshold: 3,
			// 统计周期
			StatIntervalInMs: 6000,
		},
	})

	if err != nil {
		zap.S().Error("加载流控规则失败：", err)
	}
}
