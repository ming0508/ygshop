package initialize

import "go.uber.org/zap"

func InitLogger() {
	// 执行了这一步后就可以在项目的任何地方使用zap.L()来调用全局的logger
	//logger,_:=zap.NewProduction()
	logger, _ := zap.NewDevelopment()

	//cfg := zap.NewProductionConfig()
	//cfg.OutputPaths = []string{
	//	"./myproject.log",
	//	// 以下两种输出选择的差异就是终端显示的颜色不一样（单元测试的输出里没有颜色）
	//	"stderr",
	//	//"stdout",
	//}
	//
	//logger, _ := cfg.Build()

	zap.ReplaceGlobals(logger)
}
