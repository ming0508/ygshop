package middlewares

import (
	"context"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
	"web/goods/global"
	"web/goods/proto"
)

func IsAdminAuth() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		userId, _ := ctx.Get("userId")

		zap.S().Debug("userId：", userId)

		// 这里是使用请求中的token信息来认证角色，如果用户登录后，用户的角色发生了更改，
		// 那么就必须重新登录后这里才能获取到最新的角色信息。
		// 然而实际情况是：用户角色产生变更（例如升级为会员）后，是不需要重新登录的。
		// 所以我认为这里应该根据用户id从数据库里获取角色信息。这样做就需要发个grpc调用来获取用户角色。。
		res, err := global.UserSrvClient.GetUserById(context.Background(), &proto.IdRequest{
			Id: userId.(int32),
		})
		if err != nil {
			zap.S().Error("用户服务异常:", err)
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"msg": "用户服务异常",
			})
		}

		if res.Role != 2 {
			ctx.JSON(http.StatusForbidden, gin.H{
				"msg": "权限不足",
			})
			ctx.Abort()
		}
		ctx.Next()
	}
}
