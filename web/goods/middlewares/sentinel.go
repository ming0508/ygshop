package middlewares

import (
	sentinel "github.com/alibaba/sentinel-golang/api"
	"github.com/alibaba/sentinel-golang/core/base"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 流量控制
func FlowControl() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		e, b := sentinel.Entry("get goods list", sentinel.WithTrafficType(base.Inbound))
		if b != nil {
			ctx.JSON(http.StatusTooManyRequests, gin.H{
				"msg": "请求过于频繁，请稍后再试",
			})
			ctx.Abort()
			return
		}
		ctx.Next()
		e.Exit()
	}
}
