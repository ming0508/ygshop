package forms

// 请求新建品牌的表单
type CreateBrandForm struct {
	Name string `form:"name" json:"name" binding:"max=50"`
	Logo string `form:"logo" json:"logo" binding:"url,max=200"`
}
