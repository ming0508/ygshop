package forms

// 请求新建轮播图的表单
type CreateBannerForm struct {
	Imgage string `form:"image" json:"image" binding:"url,max=200"`
	Index  int32  `form:"index" json:"index" binding:"required"`
	Url    string `form:"url" json:"url" binding:"url,max=200"`
}
