package forms

// 请求新建商品分类的表单
type CreateCategoryForm struct {
	Name   string `form:"name" json:"name" binding:"required,min=1,max=20"`
	Level  int32  `form:"level" json:"level" binding:"required"`
	Parent int32  `form:"parent" json:"parent" binding:"required"`
	IsTab  *bool  `form:"is_tab" json:"is_tab" binding:"required"`
}

// 请求修改商品分类的表单
type UpdateCategoryForm struct {
	Name   string `json:"name"`
	Level  int32  `json:"level"`
	Parent int32  `json:"parent"`
	IsTab  int32  `json:"is_tab"`
}
