package forms

// 请求新建"分类-品牌"的表单
type CreateCategorybrandForm struct {
	CategoryId int32 `form:"category_id" json:"category_id"`
	BrandId    int32 `form:"brand_id" json:"brand_id"`
}
