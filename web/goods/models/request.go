package models

import (
	"github.com/dgrijalva/jwt-go"
)

// 加解密都要使用这个结构体
type CustomClaims struct {
	ID int32 // 用户id
	jwt.StandardClaims
}
