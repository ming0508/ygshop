package models

import (
	"github.com/dgrijalva/jwt-go"
)

type CustomClaims struct {
	ID int32 // 用户id
	jwt.StandardClaims
}
