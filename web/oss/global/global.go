package global

import (
	ut "github.com/go-playground/universal-translator"
	"web/oss/config"
	"web/oss/proto"
)

var (
	Trans ut.Translator

	ServerConfig *config.ServerConfig = &config.ServerConfig{}

	NacosConfig *config.NacosConfig = &config.NacosConfig{}

	// 连接grpc服务的句柄
	UserSrvClient proto.UserClient
)
