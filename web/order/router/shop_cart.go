package router

import (
	"github.com/gin-gonic/gin"
	"web/order/api/shop_cart"
)

func InitShopCartRouter(router *gin.RouterGroup) {
	shopCartRouter := router.Group("shop_cart")
	{
		shopCartRouter.GET("", shop_cart.GetShopCartRecords)
		shopCartRouter.POST("", shop_cart.AddGoods)
		shopCartRouter.PUT(":goodId", shop_cart.UpdateCartItem)
		shopCartRouter.DELETE(":cartId", shop_cart.DeleteCartItem)
	}
}
