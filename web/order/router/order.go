package router

import (
	"github.com/gin-gonic/gin"
	"web/order/api/order"
	"web/order/api/pay"
)

func InitOrderRouter(router *gin.RouterGroup) {
	orderRouter := router.Group("order")
	{
		orderRouter.GET("", order.GetOrderList)
		orderRouter.GET(":id", order.GetOrderDetail)
		orderRouter.POST("", order.CreateOrder)
	}

	PayRouter := router.Group("pay")
	{
		PayRouter.POST("alipay/notify", pay.Notify)
	}
}
