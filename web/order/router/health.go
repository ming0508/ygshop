package router

import (
	"github.com/gin-gonic/gin"
	"web/order/api/health"
)

// 服务健康检查
func InitHealthRouter(router *gin.Engine) {
	router.GET("health", health.CheckHealth)
}
