package global

import (
	ut "github.com/go-playground/universal-translator"
	"web/order/config"
	"web/order/proto"
)

// 存放全局变量

var (
	// 配置中心nacos的连接信息
	NacosConfig = new(config.NacosConfig)
	// web层用户服务的配置信息
	ServerConfig = new(config.ServerConfig)

	// 表单验证的通用翻译器
	T ut.Translator

	// 连接grpc服务的句柄
	OrderSrvClient proto.OrderClient
)
