package health

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func CheckHealth(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"msg": "success",
	})
}
