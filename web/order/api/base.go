package api

import (
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net/http"
	"strings"
	"web/order/global"
)

// 删除掉表单验证结果的翻译结果中的点以及点之前的内容
func removeTopStruct(field map[string]string) map[string]string {
	rsp := map[string]string{}
	for field, val := range field {
		rsp[field[strings.Index(field, ".")+1:]] = val
	}
	return rsp
}

// 将grpc的code转换成http的状态码
func HandleGrpcErrorToHttp(err error, c *gin.Context) {
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.NotFound:
				c.JSON(http.StatusNotFound, gin.H{
					"msg": e.Message(),
				})
			case codes.Internal:
				c.JSON(http.StatusInternalServerError, gin.H{
					"msg": "内部错误: " + e.Message(),
				})
			case codes.InvalidArgument:
				c.JSON(http.StatusBadRequest, gin.H{
					"msg": "参数错误: " + e.Message(),
				})
			case codes.Unavailable:
				c.JSON(http.StatusBadRequest, gin.H{
					"msg": "service层服务不可用: " + e.Message(),
				})
			default:
				c.JSON(http.StatusInternalServerError, gin.H{
					"msg": "其他错误: " + e.Message(),
				})
			}
			return
		}
	}
}

// 将表单验证的错误信息翻译为中文
func HandleValidatorError(ctx *gin.Context, err error) {
	// 将表单验证的错误信息翻译为中文
	errs, ok := err.(validator.ValidationErrors)
	if !ok {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
	}

	ctx.JSON(http.StatusBadRequest, gin.H{
		// Translate()的参数是一个通用翻译器
		"error": removeTopStruct(errs.Translate(global.T)),
	})
	return
}

// 获取当前服务的起始span，该span在gin服务的组件中创建，并保存在每一次请求的ctx中
func GetTopSpan(ctx *gin.Context) opentracing.Span {
	topSpan, ok := ctx.Get("topSpan")
	if !ok {
		zap.S().Error("获取起始span时出错")
	}
	return topSpan.(opentracing.Span)
}
