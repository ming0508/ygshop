package initialize

import (
	"fmt"
	_ "github.com/mbobakov/grpc-consul-resolver"
	otgrpc "github.com/opentracing-contrib/go-grpc"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"web/order/global"
)

// 同时完成了服务发现和负载均衡算法（轮询）
func InitSrvConn(srvName string) *grpc.ClientConn {
	consulInfo := global.ServerConfig.ConsulInfo

	// 声明客户端拦截器列表interceptors
	var interceptors []grpc.UnaryClientInterceptor
	//opentracing.GlobalTracer().
	interceptors = append(interceptors, otgrpc.OpenTracingClientInterceptor(opentracing.GlobalTracer()))
	// 添加其他拦截器
	//interceptors = append(interceptors, )

	conn, err := grpc.Dial(
		fmt.Sprintf("consul://%s:%d/%s?healthy=true&wait=14s&tag=yg_shop",
			consulInfo.Host, consulInfo.Port, srvName),
		grpc.WithInsecure(),
		// grpc目前似乎只支持轮询的负载均衡算法
		grpc.WithDefaultServiceConfig(`{"loadBalancingPolicy": "round_robin"}`),
		grpc.WithChainUnaryInterceptor(interceptors...),
	)
	if err != nil {
		zap.S().Error("service层服务发现出错: ", err)
		return nil
	}
	return conn
}
