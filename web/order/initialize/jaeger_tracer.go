package initialize

import (
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	"io"
)

func InitJaegerTracer() (opentracing.Tracer, io.Closer, error) {
	// 初始化链路追踪的基本配置
	cfg := jaegercfg.Configuration{
		Sampler: &jaegercfg.SamplerConfig{
			// 全量采集
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans:           true,
			LocalAgentHostPort: "127.0.0.1:6831",
		},
		ServiceName: "ygshop-web-order",
	}

	// 根据链路追踪配置生成新的tracer
	return cfg.NewTracer(jaegercfg.Logger(jaeger.StdLogger))
}
