package middlewares

import (
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
)

func StartSpan() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// 每次请求当前服务时，在中间件中创建一个起始span，并保存在当前请求的ctx中
		span := opentracing.GlobalTracer().StartSpan(ctx.Request.URL.Path + " [" + ctx.Request.Method + "]")
		defer span.Finish()
		ctx.Set("topSpan", span)
		ctx.Next()
	}
}
