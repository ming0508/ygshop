package model

import (
	"crypto/sha512"
	"fmt"
	"github.com/anaskhan96/go-password-encoder"
	"strings"
	"testing"
)

// 用户密码加解密测试

func TestPwd(t *testing.T) {
	// 生成盐的规则
	options1 := &password.Options{16, 100, 32, sha512.New}
	// 获取盐值以及原始密码加盐再加密后的密文
	salt, encodedPwd := password.Encode("238ur2f-238r--kfih8394", options1)
	// 密码长度不应大于字段定义的100长度
	// 将加密方法、生成盐的方法、盐值、最终密文都保存在用户密码中
	newPassword := fmt.Sprintf("$pbkdf2-sha512$%s$%s", salt, encodedPwd)
	t.Log(newPassword)
	pwdInfo := strings.Split(newPassword, "$")

	// 用户登录时取出数据库中存储的pwdInfo, 再取出其中的盐值、密文，再和登录时输入的密码进行比对
	options2 := &password.Options{16, 100, 32, sha512.New}
	if password.Verify("238ur2f-238r--kfih8394", pwdInfo[2], pwdInfo[3], options2) {
		t.Log("验证成功")
	} else {
		t.Log("验证失败")
	}
}
