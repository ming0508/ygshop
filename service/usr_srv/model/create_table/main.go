package main

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"log"
	"os"
	"service/usr_srv/model"
	"time"
)

// 同步user表结构（生成user表）

func main() {
	dsn := "root:root@tcp(localhost:3306)/ygshop_user_srv?charset=utf8mb4&parseTime=True&loc=Local"

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags),
		logger.Config{
			SlowThreshold: time.Second, //慢sql阈值
			LogLevel:      logger.Info, // Log level
			Colorful:      true,        // 是否彩色打印
		},
	)

	// 全局模式
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		// 这里是为了让生成的表名末尾不会自动添加s
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},

		Logger: newLogger,
	})
	if err != nil {
		panic("连接数据库失败：" + err.Error())
	}

	// 程序执行到这里时，终端会打印出sql语句
	db.AutoMigrate(&model.User{})
}
