package handler

import (
	"context"
	"fmt"
	. "service/usr_srv/global"
	"service/usr_srv/initialize"
	"service/usr_srv/model"
	pt "service/usr_srv/proto"
	"testing"
)

func TestUserServer_GetUserListV1(t *testing.T) {
	// 获取全部记录
	var users []model.User
	result := DB.Offset(1).Limit(1).Find(&users)
	// SELECT * FROM users;

	t.Logf("%#v\n", users)
	t.Log(result.RowsAffected) // 返回找到的记录数，相当于 `len(users)`
}

// 接口测试
func TestUserServer_GetUserListV2(t *testing.T) {
	us := new(UserServer)
	pageInfo := pt.PageInfo{
		Pn:    0,
		PSize: 0,
	}
	userListResponse, err := us.GetUserList(context.Background(), &pageInfo)
	if err != nil {
		t.Error(err)
	}
	t.Log(userListResponse.Total)
	for _, v := range userListResponse.Data {
		t.Logf("---------")
		t.Log(v.Id)
		t.Log(v.Birthday)
	}
}

func TestUserServer_GetUserByMobile(t *testing.T) {
	us := new(UserServer)
	r := &pt.MobileRequest{Mobile: "17610490508"}
	res, _ := us.GetUserByMobile(context.Background(), r)
	t.Log(res.Birthday)
}

func TestUserServer_GetUserById(t *testing.T) {
	initialize.InnitConfig()
	initialize.InitLogger()
	initialize.InitDB()
	us := new(UserServer)
	r := &pt.IdRequest{Id: 124318}
	res, err := us.GetUserById(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res.Birthday)
}

func TestUserServer_CreateUser(t *testing.T) {
	initialize.InitLogger()
	initialize.InnitConfig()
	initialize.InitDB()
	us := new(UserServer)
	r := &pt.CreateUserInfo{
		Mobile:   "13766666666",
		Password: "admin123",
		NickName: "周杰伦",
	}
	res, err := us.CreateUser(context.Background(), r)
	if err != nil {
		fmt.Println(err)
		return
	}
	t.Log(res.Password)
}

func TestUserServer_UpdateUser(t *testing.T) {
	us := new(UserServer)
	r := &pt.UpdateUserInfo{
		Id:       2143124,
		NickName: "蔡依林",
		Gender:   "famale",
		Birthday: 683519175,
	}
	_, err := us.UpdateUser(context.Background(), r)
	if err != nil {
		fmt.Println(err)
		return
	}
	t.Logf("更新成功")
}

func TestUserServer_CheckPassword(t *testing.T) {
	us := new(UserServer)
	r := &pt.PasswordCheckInfo{
		Password:          "238ur2f-238r--kfih8394",
		EncryptedPassword: "$pbkdf2-sha512$7UYlpDb5FZQRTldc$3e79179bb3834b0f87d752c9107ef02578e5262abefcfde2835f40542aad8bd6",
	}
	res, _ := us.CheckPassword(context.Background(), r)
	t.Log(res.Success)
}
