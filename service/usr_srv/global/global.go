package global

import (
	"gorm.io/gorm"
	"service/usr_srv/config"
)

var (
	// 全局句柄
	DB *gorm.DB
	// service层用户服务的配置信息
	ServerConfig = new(config.ServerConfig)
	// 配置中心nacos的连接信息
	NacosConfig = new(config.NacosConfig)
)
