module service

go 1.16

require (
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/anaskhan96/go-password-encoder v0.0.0-20201010210601-c765b799fd72
	github.com/apache/rocketmq-client-go/v2 v2.1.0
	github.com/go-redis/redis/v8 v8.6.0
	github.com/go-redsync/redsync/v4 v4.0.4
	github.com/golang/protobuf v1.5.2
	github.com/hashicorp/consul/api v1.10.1
	github.com/mbobakov/grpc-consul-resolver v1.4.4
	github.com/nacos-group/nacos-sdk-go v1.0.8
	github.com/olivere/elastic/v7 v7.0.29
	github.com/opentracing-contrib/go-grpc v0.0.0-20210225150812-73cb765af46e
	github.com/opentracing/opentracing-go v1.2.0
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/viper v1.8.1
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	go.uber.org/zap v1.17.0
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.14
)
