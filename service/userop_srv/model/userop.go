package model

const (
	LEAVING_MESSAGES = iota + 1
	COMPLAINT
	INQUIRY
	POST_SALE
	WANT_TO_BUY
)

// 用户留言表
type LeavingMessages struct {
	BaseModel
	User        int32  `gorm:"type:int;index;comment:用户id"`
	MessageType int32  `gorm:"type:int;comment:留言类型: 1(留言),2(投诉),3(询问),4(售后),5(求购)"`
	Subject     string `gorm:"type:varchar(100);comment:留言主题"`
	Message     string `gorm:"comment:留言内容"` // 默认为text类型
	File        string `gorm:"type:varchar(200);comment:存储的是oss服务中访问这个文件的url"`
}

// 用户的收货地址
type Address struct {
	BaseModel
	User         int32  `gorm:"type:int;index;comment:用户id"`
	Province     string `gorm:"type:varchar(10);comment:地址所在省份"`
	City         string `gorm:"type:varchar(10);comment:地址所在城市"`
	District     string `gorm:"type:varchar(20);comment:地址所在区"`
	Address      string `gorm:"type:varchar(100);comment:地址详情"`
	SignerName   string `gorm:"type:varchar(20);comment:收货人的名称"`
	SignerMobile string `gorm:"type:varchar(11);comment:收货人的手机号码"`
}

// 用户收藏表
type UserFav struct {
	BaseModel
	// index:idx_user_goods,unique 指定了联合唯一索引，这样一来，一组用户id和商品id就只能出现一次
	User  int32 `gorm:"type:int;index:idx_user_goods,unique;comment:用户id"`
	Goods int32 `gorm:"type:int;index:idx_user_goods,unique;comment:商品id"`
}
