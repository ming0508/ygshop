package handler

import (
	"context"
	"service/userop_srv/initialize"
	pt "service/userop_srv/proto"
	"testing"
)

func Init() {
	initialize.InitLogger()
	initialize.InnitConfig()
	initialize.InitDB()
}

func TestUseropServer_CreateMessage(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.MessageRequest{
		UserId:      2143136,
		MessageType: 4,
		Subject:     "留言主题测试",
		Message:     "留言内容测试",
		File:        "https://89234j.832u.com",
	}
	res, err := us.CreateMessage(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("创建成功")
	t.Log("留言记录id：", res.Id)
}

func TestUseropServer_MessageList(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.MessageRequest{
		UserId: 2143136,
	}
	res, err := us.MessageList(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res.Total)
	for _, v := range res.Data {
		t.Logf("%#v\n", v)
	}
}

func TestUseropServer_CreateAddress(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.AddressRequest{
		UserId:       214313666,
		Province:     "山西",
		City:         "阳泉",
		District:     "测试区",
		Address:      "测试地址3",
		SignerName:   "李彦宏",
		SignerMobile: "13666666666",
	}
	res, err := us.CreateAddress(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("创建成功")
	t.Log("收货地址id：", res.Id)
}

func TestUseropServer_GetAddressList(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.AddressRequest{
		UserId: 2143136,
	}
	res, err := us.GetAddressList(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res.Total)
	for _, v := range res.Data {
		t.Logf("%#v\n", v)
	}
}

func TestUseropServer_UpdateAddress(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.AddressRequest{
		Id:           3,
		Province:     "山西555",
		City:         "阳泉666",
		District:     "测试区777",
		Address:      "测试地址3888",
		SignerName:   "李彦宏999",
		SignerMobile: "13666666111",
	}
	_, err := us.UpdateAddress(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("修改成功")
}

func TestUseropServer_DeleteAddress(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.AddressRequest{
		Id: 3,
	}
	_, err := us.DeleteAddress(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("删除成功")
}

func TestUseropServer_AddUserFav(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.UserFavRequest{
		UserId:  214313666,
		GoodsId: 36,
	}
	_, err := us.AddUserFav(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("创建成功")
}

func TestUseropServer_GetFavList(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.UserFavRequest{
		UserId:  214313666,
		GoodsId: 35,
	}
	res, err := us.GetFavList(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res.Total)
	for _, v := range res.Data {
		t.Logf("%#v\n", v)
	}
}

func TestUseropServer_GetUserFavDetail(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.UserFavRequest{
		UserId:  214313666,
		GoodsId: 359,
	}
	_, err := us.GetUserFavDetail(context.Background(), r)
	if err != nil {
		t.Log("用户未收藏过该商品")
		return
	}
	t.Log("用户已收藏过该商品")
}

func TestUseropServer_DeleteUserFav(t *testing.T) {
	Init()
	us := new(UseropServer)
	r := &pt.UserFavRequest{
		UserId:  214313666,
		GoodsId: 359,
	}
	_, err := us.DeleteUserFav(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("删除成功")
}
