package handler

import (
	"context"
	"github.com/apache/rocketmq-client-go/v2/primitive"
	"go.uber.org/zap"
	"service/inventory_srv/global"
	"service/inventory_srv/initialize"
	"service/inventory_srv/model"
	pt "service/inventory_srv/proto"
	"sync"
	"testing"
)

func Init() {
	initialize.InitLogger()
	initialize.InnitConfig()
	initialize.InitDB()
}

func TestGoodsServer_SetInv(t *testing.T) {
	Init()
	us := new(InventoryServer)
	r := &pt.GoodsInvInfo{GoodsId: 124, Num: 80}
	_, err := us.SetInv(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("更新成功")
}

func TestGoodsServer_GetGoodsInvInfo(t *testing.T) {
	Init()
	us := new(InventoryServer)
	r := &pt.GoodsInvInfo{GoodsId: 123}
	res, err := us.InvDetail(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res)
}

func TestGoodsServer_Sell(t *testing.T) {
	Init()
	us := new(InventoryServer)
	r := &pt.SellInfo{GoodsInfo: []*pt.GoodsInvInfo{
		{GoodsId: 123, Num: 10},
		{GoodsId: 124, Num: 10},
	}}
	_, err := us.Sell(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("扣减成功")
}

// 为所有商品添加库存
func TestGoodsServer_UpdateInventory(t *testing.T) {
	Init()
	for i := 1; i < 62; i++ {
		us := new(InventoryServer)
		r := &pt.GoodsInvInfo{GoodsId: int32(i), Num: 100}
		_, err := us.SetInv(context.Background(), r)
		if err != nil {
			t.Fatal(err)
		}
	}

	t.Log("添加成功")
}

// 复现大并发场景下扣减库存导致数据不一致的问题
func TestGoodsServer_Problem(t *testing.T) {
	Init()
	wg := new(sync.WaitGroup)
	n := 20
	wg.Add(n)
	for i := 0; i < n; i++ {
		go SellOne(wg, t)
	}

	wg.Wait()
	t.Log("扣减结束")
}

func SellOne(wg *sync.WaitGroup, t *testing.T) {
	defer wg.Done()
	us := new(InventoryServer)
	r := &pt.SellInfo{GoodsInfo: []*pt.GoodsInvInfo{
		{GoodsId: 1, Num: 1},
		{GoodsId: 2, Num: 1},
	}}
	_, err := us.Sell(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("扣减成功")
}

// 根据多个goodsId查询库存记录
func TestGetInvByGoodsIds(t *testing.T) {
	Init()
	var invs []*model.Inventory
	ids := []int{55, 56, 57}
	global.DB.Where("goods IN ?", ids).Find(&invs)
	for _, v := range invs {
		t.Logf("%#v\n", v)
	}
}

func TestUpdateGoodsInv(t *testing.T) {
	Init()
	sql := "insert into inventory (goods,stocks) values (58,500),(60,500),(59,500) on duplicate key update stocks=values(stocks);"
	if updateRes := global.DB.Exec(sql); updateRes.Error != nil {
		zap.S().Error("归还库存的sql执行出错", updateRes.Error)
	}
}

func TestAutoReback(t *testing.T) {
	Init()
	m := &primitive.MessageExt{}
	m.Body = []byte("202111181645455244946950001")
	AutoReback(context.Background(), m)
}

/*
   inventory_test.go:138: 协程1收到最接ip库信息：ipranges_20211120_155252
   inventory_test.go:138: 协程4收到最接ip库信息：ipranges_20211120_155252
   inventory_test.go:138: 协程3收到最接ip库信息：ipranges_20211120_155252
   inventory_test.go:138: 协程0收到最接ip库信息：ipranges_20211120_155252
   inventory_test.go:138: 协程2收到最接ip库信息：ipranges_20211120_155252
*/
func TestSubscribe(t *testing.T) {
	Init()
	for i := 0; i < 5; i++ {
		go func(i int) {
			pb := global.RedisClient.Subscribe(context.Background(), "[letv-AD]current_iplib")
			t.Logf("协程%d正在订阅消息", i)
			for {

				select {
				case mg := <-pb.Channel():
					if mg.Payload != "close" {
						t.Logf("协程%d收到最接ip库信息：%v", i, mg.Payload)
					}
				}
			}
		}(i)
	}
	select {}
}

func TestPublish(t *testing.T) {
	Init()
	//global.RedisClient.Publish(context.Background(),"current_iplib","[letv-AD]current_iplib")
	global.RedisClient.Publish(context.Background(), "current_iplib", " ipranges_20210330_1055")
}
