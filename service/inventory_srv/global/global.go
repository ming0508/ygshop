package global

import (
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
	"service/inventory_srv/config"
)

var (
	// 全局句柄
	DB *gorm.DB
	// service层用户服务的配置信息
	ServerConfig = new(config.ServerConfig)
	// 配置中心nacos的连接信息
	NacosConfig = new(config.NacosConfig)

	RedisClient *redis.Client
)
