package redsync

import (
	"errors"
	"fmt"
	goredislib "github.com/go-redis/redis/v8"
	"github.com/go-redsync/redsync/v4"
	"github.com/go-redsync/redsync/v4/redis/goredis/v8"
	"sync"
	"testing"
	"time"
)

func Test_redsync(t *testing.T) {
	// Create a pool with go-redis (or redigo) which is the pool redisync will
	// use while communicating with Redis. This can also be any pool that
	// implements the `redis.Pool` interface.
	client := goredislib.NewClient(&goredislib.Options{
		Addr: "127.0.0.1:6379",
	})
	pool := goredis.NewPool(client) // or, pool := redigo.NewPool(...)

	// Create an instance of redisync to be used to obtain a mutual exclusion
	// lock.
	rs := redsync.New(pool)

	// Obtain a new mutex by using the same name for all instances wanting the
	// same lock.
	mutexname := "my-global-mutex"

	wg := new(sync.WaitGroup)
	num := 5
	wg.Add(num)

	for i := 0; i < num; i++ {
		fmt.Println(i)
		go func(i int) {
			defer wg.Done()
			fmt.Printf("协程 %d 正在尝试获取全局锁\n", i)
			// redis中的key存在期间可保证并发安全
			//mutex := rs.NewMutex(mutexname)

			// 默认重试32次，每次失败后等待500毫秒，也就是说，等待16秒后将报错——获取锁失败
			// 第1个g获取锁并占用5秒，释放锁
			// 第2个g等待5秒，获取锁并占用5秒，释放锁
			// 第3个g等待10秒，获取锁并占用5秒，释放锁
			// 第4个g等待15秒，获取锁并占用5秒，释放锁
			// 第5个g需要等待20秒，才能获取到锁，但是在第16秒就报错了，因为超过了最大的尝试时间——32*0.5

			// 如果redis中已经有同名的锁，那么当前的g会一直处于等待状态。
			// 当前g释放锁后，相应的key会从redis中删除
			// 当前g获取到锁后，会向redis中写入一个key，并且重置过期时间

			mutex := rs.NewMutex(mutexname)
			//mutex := rs.NewMutex(mutexname, redsync.WithExpiry(10000000*time.Second))
			//	redsync.WithRetryDelay(4*time.Second))
			if err := mutex.Lock(); err != nil {
				panic(errors.New(fmt.Sprintf("协程%d获取全局锁失败:%v", i, err.Error())))
			}
			fmt.Printf("协程 %d 获取全局锁成功\n", i)
			fmt.Printf("协程 %d 正在执行任务\n", i)
			time.Sleep(time.Second * 5)
			if ok, err := mutex.Unlock(); !ok || err != nil {
				panic("unlock failed")
			}
			fmt.Printf("协程 %d 已将锁释放\n", i)
		}(i)
	}

	wg.Wait()
}
