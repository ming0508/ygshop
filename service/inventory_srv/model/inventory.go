package model

// 库存表
// 后续可能要经常根据商品id来查询库存，所以为Goods字段添加了索引"index"
type Inventory struct {
	BaseModel
	Goods   int32 `gorm:"type:int;index:idx_inventory_goods,unique;comment:商品id"`
	Stocks  int32 `gorm:"type:int;comment:商品库存"`
	Version int32 `gorm:"type:int;comment:与分布式锁的乐观锁相关"`
}

// 库存扣减详情
type StockSellDetail struct {
	// 建立索引，值唯一
	OrderSn string `gorm:"type:varchar(200);index:idx_order_sn,unique;comment:订单编号"`
	// 订单的库存扣减或者归还后都要更新这个字段，
	// 执行库存归还前一定要判断这个状态是否为"已扣减"，
	// 只有"已扣减"状态的订单才可以执行库存归还。
	Status int `gorm:"type:int;comment:库存扣减状态。1：已扣减。2：已归还"`
	// 详细记录这个订单下各个商品扣减之前的库存，至于为什么不把这个字段拆解成Goods和Nums，
	// 是因为拆解后每当对一个订单执行库存扣减时，就需要更新多条记录。
	Detail GoodsDetailList `gorm:"type:varchar(200);comment:该订单下各个商品执行扣减之前的库存"`
}

//// 库存变更历史表
//type InventoryHistory struct {
//	User int32
//	Goods int32
//	Nums int32
//	Order int32
//	Status int32 // 1 表示库存为预扣减状态，2 表示订单已经支付
//}
