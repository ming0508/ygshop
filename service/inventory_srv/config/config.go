package config

type RedisConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
}

type MysqlConfig struct {
	Host     string `mapstructure:"host" json:"host"`
	Port     int    `mapstructure:"port" json:"port"`
	Name     string `mapstructure:"db" json:"db"`
	User     string `mapstructure:"user" json:"user"`
	Password string `mapstructure:"password" json:"password"`
}

type ConsulConfig struct {
	Host string `mapstructure:"host" json:"host"`
	Port int    `mapstructure:"port" json:"port"`
}

type ServerConfig struct {
	Name         string       `mapstructure:"name" json:"name"`                   // 服务发现的名称
	Tags         []string     `mapstructure:"tags" json:"tags"`                   // 注册服务时发送的服务标签
	RegisterHost string       `mapstructure:"register_host" json:"register_host"` // 注册服务时发送的ip地址
	RedisInfo    RedisConfig  `mapstructure:"redis" json:"redis"`
	MysqlInfo    MysqlConfig  `mapstructure:"mysql" json:"mysql"`
	ConsulInfo   ConsulConfig `mapstructure:"consul" json:"consul"`
}

type NacosConfig struct {
	IpAddr    string `mapstructure:"ip_addr"`
	Port      uint64 `mapstructure:"port"`
	UserName  string `mapstructure:"username"`
	Password  string `mapstructure:"password"`
	Namespace string `mapstructure:"namespace_id"`
	DataId    string `mapstructure:"data_id"`
	Group     string `mapstructure:"group"`
}
