package handler

import (
	"context"
	"google.golang.org/protobuf/types/known/emptypb"
	pt "service/goods_srv/proto"
	"testing"
)

/*
1：https://item.jd.com/65686973921.html
2：https://item.jd.com/100008565426.html
3：https://item.jd.com/10020158103186.html
4：https://item.jd.com/100005368774.html
*/
func TestGoodsServer_CreateBanner(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.BannerRequest{
		Index: 4,
		Image: "https://img14.360buyimg.com/n5/jfs/t1/52974/1/102/195682/5cd29b6aEc59c8f30/20919ac7ba0d61a3.jpg",
		Url:   "/app/home/productDetail/4",
	}
	res, err := us.CreateBanner(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%#v", res)
}

func TestGoodsServer_GetBannerList(t *testing.T) {
	Init()
	us := new(GoodsServer)
	res, err := us.BannerList(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(res.Total)
	for _, v := range res.Data {
		t.Logf("---------")
		t.Log(v.Id)
		t.Log(v.Index)
		t.Log(v.Image)
		t.Log(v.Url)
	}
}

func TestGoodsServer_DeleteBanner(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.BannerRequest{
		Id: 1,
	}
	_, err := us.DeleteBanner(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("删除成功")
}

func TestGoodsServer_UpdateBanner(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.BannerRequest{
		Id:    1,
		Index: 1,
		Image: "https://img10.360buyimg.com/imgzone/jfs/t1/105207/3/12736/675086/5e4e5142E25cd996a/07ec5c3b5f3378b7.jpg",
		Url:   "/app/home/productDetail/1",
	}
	_, err := us.UpdateBanner(context.Background(), r)
	if err != nil {
		t.Error(err)
	}
	t.Log("更新成功")
}
