package handler

import (
	"context"
	"service/goods_srv/initialize"
	pt "service/goods_srv/proto"
	"testing"
)

func Init() {
	initialize.InitLogger()
	initialize.InnitConfig()
	initialize.InitDB()
}

func TestGoodsServer_CreateBrand(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.BrandRequest{
		Name: "寰球渔市",
	}
	res, err := us.CreateBrand(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%#v", res)
}

func TestGoodsServer_GetBrandList(t *testing.T) {
	Init()
	us := new(GoodsServer)
	pageInfo := pt.BrandFilterRequest{
		Pages:       1,
		PagePerNums: 5,
	}
	goodsListResponse, err := us.BrandList(context.Background(), &pageInfo)
	if err != nil {
		t.Error(err)
	}
	t.Log(goodsListResponse.Total)
	for _, v := range goodsListResponse.Data {
		t.Logf("---------")
		t.Log(v.Id)
		t.Log(v.Name)
		t.Log(v.Logo)
	}
}

func TestGoodsServer_UpdateBrand(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.BrandRequest{
		Id:   2,
		Name: "京觅",
		Logo: "https://img30.360buyimg.com/popshop/jfs/t1/5871/24/8604/4075/5baa0148E93f1a75f/3ea946875c373491.jpg",
	}
	_, err := us.UpdateBrand(context.Background(), r)
	if err != nil {
		t.Error(err)
	}
	t.Log("更新成功")
}

func TestGoodsServer_DeleteBrand(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.BrandRequest{
		Id: 2,
	}
	_, err := us.DeleteBrand(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("删除成功")
}
