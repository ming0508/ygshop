package handler

import (
	"context"
	pt "service/goods_srv/proto"
	"testing"
)

func TestGoodsServer_CreateCategoryBrand(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CategoryBrandRequest{
		CategoryId: 33,
		BrandId:    73,
	}
	res, err := us.CreateCategoryBrand(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%#v", res.Category)
	t.Logf("%#v", res.Brand)
}

func TestGoodsServer_CategoryBrandList(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CategoryBrandFilterRequest{
		Pages:       1,
		PagePerNums: 10,
	}
	res, err := us.CategoryBrandList(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(res.Total)
	t.Log(res.Data)
}

func TestGoodsServer_GetCategoryBrandList(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CategoryInfoRequest{
		Id: 21,
	}
	res, err := us.GetCategoryBrandList(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(res.Total)
	for _, v := range res.Data {
		t.Log("-----------")
		t.Log(v.Id)
		t.Log(v.Name)
		t.Log(v.Logo)
	}
}

func TestGoodsServer_DeleteCategoryBrand(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CategoryBrandRequest{
		Id: 3,
	}
	_, err := us.DeleteCategoryBrand(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("删除成功")
}

func TestGoodsServer_UpdateCategoryBrand(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CategoryBrandRequest{
		Id:         3,
		CategoryId: 21,
		BrandId:    1,
	}
	_, err := us.UpdateCategoryBrand(context.Background(), r)
	if err != nil {
		t.Error(err)
	}
	t.Log("更新成功")
}
