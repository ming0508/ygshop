package handler

import (
	"context"
	"google.golang.org/protobuf/types/known/emptypb"
	pt "service/goods_srv/proto"
	"testing"
)

func TestGoodsServer_CreateCategory(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CategoryInfoRequest{
		Name:           "鸡枞菌",
		ParentCategory: 20,
		Level:          3,
		IsTab:          false,
	}
	res, err := us.CreateCategory(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%#v", res)
}

func TestGoodsServer_GetAllCategorysList(t *testing.T) {
	Init()
	us := new(GoodsServer)
	res, err := us.GetAllCategorysList(context.Background(), &emptypb.Empty{})
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%v", res.JsonData)
}

func TestGoodsServer_GetSubCategory(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CategoryListRequest{
		Id: 9,
	}
	res, err := us.GetSubCategory(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%v", res)
	t.Logf("%v", res.Info)
	t.Logf("%v", res.SubCategorys)
}

func TestGoodsServer_UpdateCategory(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CategoryInfoRequest{
		Id:             65,
		Name:           "鸡枞菌",
		ParentCategory: 20,
		Level:          3,
		IsTab:          false,
	}
	_, err := us.UpdateCategory(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("更新成功")
}
