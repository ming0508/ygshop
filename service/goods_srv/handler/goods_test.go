package handler

import (
	"context"
	"google.golang.org/grpc"
	"service/goods_srv/global"
	"service/goods_srv/initialize"
	pt "service/goods_srv/proto"
	"testing"
)

// 测试直连inventory，不使用负载均衡
func TestGoodsServer_CreateGoodsStraight(t *testing.T) {
	conn, err := grpc.Dial(":63165", grpc.WithInsecure())
	if err != nil {
		panic("grpc连接失败：" + err.Error())
	}

	defer conn.Close()

	client := pt.NewInventoryClient(conn)

	_, err = client.SetInv(context.Background(), &pt.GoodsInvInfo{
		GoodsId: 1,
		Num:     500,
	})
	if err != nil {
		t.Fatal(err)
	}
}

func TestGoodsServer_CreateGoods(t *testing.T) {
	Init()
	if conn := initialize.InitSrvConn(global.ServerConfig.InventorySrvInfo.Name); conn != nil {
		global.InventorySrvClient = pt.NewInventoryClient(conn)
	}
	us := new(GoodsServer)
	r := &pt.CreateGoodsInfo{
		BrandId:         70,
		CategoryId:      64,
		Name:            "玉翔 现货香格里拉松茸新鲜非干货特级精品煲汤火锅食材菇菌3-5cm 500克实惠装",
		GoodsBrief:      "玉翔 现货香格里拉松茸新鲜非干货特级精品煲汤火锅食材菇菌3-5cm 500克实惠装",
		MarketPrice:     218.00,
		ShopPrice:       218.00,
		GoodsSn:         "100012407559",
		ShipFree:        false,
		GoodsFrontImage: "https://img14.360buyimg.com/n5/jfs/t1/177982/18/11613/139994/60dec27cE0661c9d5/7eb78ec95a06b7cc.jpg",
		Images: []string{
			"https://img14.360buyimg.com/n5/jfs/t1/177982/18/11613/139994/60dec27cE0661c9d5/7eb78ec95a06b7cc.jpg",
			"https://img14.360buyimg.com/n5/jfs/t1/177271/38/12148/213864/60dec299Eccc4cb9d/dd3d0c370740d258.jpg",
			"https://img14.360buyimg.com/n5/jfs/t1/20150/34/17291/286076/60dec2a3E92865f76/c1a6f308a68fef4d.jpg",
			"https://img14.360buyimg.com/n5/jfs/t1/174948/38/17826/143594/60dec2d3E10d3e109/26f12efa3975024e.jpg",
		},
		DescImages: []string{
			"https://img30.360buyimg.com/sku/jfs/t1/197585/13/3791/851558/611c8376E26c4b7e8/5830e52b6a9e38b8.jpg",
			"https://img30.360buyimg.com/sku/jfs/t1/6163/29/15685/625859/60dec325E671eb02f/fa721858065ef75d.jpg",
			"https://img30.360buyimg.com/sku/jfs/t1/178636/9/12264/313181/60dec325Eed6157fe/2d9ed5774ed16f85.jpg",
		},
		IsNew:  true,
		IsHot:  false,
		OnSale: true,
		Stocks: 700,
	}
	res, err := us.CreateGoods(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%#v", res)
}

func TestGoodsServer_GoodsList(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.GoodsFilterRequest{
		//TopCategory: 21,
		//PriceMax: 60,

		//Brand: 2,

		//KeyWords: "京东",

		IsNew: true,
	}

	res, err := us.GoodsList(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("res.Total:", res.Total)
	for _, v := range res.Data {
		t.Log("------")
		t.Log(v.Id)
		t.Log(v.Name)
		t.Log(v.Category)
		t.Log(v.Brand)
		t.Log(v.ShopPrice)
		t.Log(v.Images)
		t.Log(v.DescImages)
	}
}

func TestGoodsServer_BatchGetGoods(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.BatchGoodsIdInfo{
		Id: []int32{1, 2, 3},
	}

	res, err := us.BatchGetGoods(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("res.Total:", res.Total)
	for _, v := range res.Data {
		t.Log("------")
		t.Log(v.Id)
		t.Log(v.Name)
		t.Log(v.Category)
		t.Log(v.Brand)
		t.Log(v.ShopPrice)
	}
}

func TestGoodsServer_GetGoodsDetail(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.GoodInfoRequest{
		Id: 666,
	}

	res, err := us.GetGoodsDetail(context.Background(), r)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(res.Id)
	t.Log(res.Name)
	t.Log(res.Category)
	t.Log(res.Brand)
	t.Log(res.ShopPrice)
}

func TestGoodsServer_DeleteGoods(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.DeleteGoodsInfo{
		Id: 1,
	}
	_, err := us.DeleteGoods(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("删除成功")
}

func TestGoodsServer_UpdateGoods(t *testing.T) {
	Init()
	us := new(GoodsServer)
	r := &pt.CreateGoodsInfo{
		Id:              61,
		BrandId:         70,
		CategoryId:      64,
		Name:            "玉翔 现货香格里拉松茸新鲜非干货特级精品煲汤火锅食材菇菌3-5cm 500克实惠装",
		GoodsBrief:      "玉翔 现货香格里拉松茸新鲜非干货特级精品煲汤火锅食材菇菌3-5cm 500克实惠装",
		MarketPrice:     218.00,
		ShopPrice:       218.00,
		GoodsSn:         "100012407559",
		ShipFree:        false,
		GoodsFrontImage: "https://img14.360buyimg.com/n5/jfs/t1/177982/18/11613/139994/60dec27cE0661c9d5/7eb78ec95a06b7cc.jpg",
		Images: []string{
			"https://img14.360buyimg.com/n5/jfs/t1/177982/18/11613/139994/60dec27cE0661c9d5/7eb78ec95a06b7cc.jpg",
			"https://img14.360buyimg.com/n5/jfs/t1/177271/38/12148/213864/60dec299Eccc4cb9d/dd3d0c370740d258.jpg",
			"https://img14.360buyimg.com/n5/jfs/t1/20150/34/17291/286076/60dec2a3E92865f76/c1a6f308a68fef4d.jpg",
			"https://img14.360buyimg.com/n5/jfs/t1/174948/38/17826/143594/60dec2d3E10d3e109/26f12efa3975024e.jpg",
			//"https://img13.360buyimg.com/n5/jfs/t1/152333/13/14424/104387/5ffd6366Ebe362e09/9488d41858dc90fb.jpg",
		},
		DescImages: []string{
			"https://img30.360buyimg.com/sku/jfs/t1/197585/13/3791/851558/611c8376E26c4b7e8/5830e52b6a9e38b8.jpg",
			"https://img30.360buyimg.com/sku/jfs/t1/6163/29/15685/625859/60dec325E671eb02f/fa721858065ef75d.jpg",
			"https://img30.360buyimg.com/sku/jfs/t1/178636/9/12264/313181/60dec325Eed6157fe/2d9ed5774ed16f85.jpg",
		},
		IsNew:  true,
		IsHot:  false,
		OnSale: true,
	}
	_, err := us.UpdateGoods(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("更新成功")
}
