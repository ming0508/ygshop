package global

import (
	"github.com/olivere/elastic/v7"
	"gorm.io/gorm"
	"service/goods_srv/config"
	"service/goods_srv/proto"
)

var (
	// 全局句柄
	DB *gorm.DB
	// service层用户服务的配置信息
	ServerConfig = new(config.ServerConfig)
	// 配置中心nacos的连接信息
	NacosConfig = new(config.NacosConfig)
	// 连接grpc服务的句柄
	InventorySrvClient proto.InventoryClient

	EsClient *elastic.Client
)
