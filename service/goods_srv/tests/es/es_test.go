package es

import (
	"context"
	"encoding/json"
	"github.com/olivere/elastic/v7"
	"log"
	"os"
	"service/goods_srv/model"
	"testing"
	"time"
)

type Account struct {
	AccountNumber int    `json:"account_number"`
	Balance       int    `json:"balance"`
	Firstname     string `json:"firstname"`
	Lastname      string `json:"lastname"`
	Age           int    `json:"age"`
	Gender        byte   `json:"gender"`
	Address       string `json:"address"`
	Employer      string `json:"employer"`
	Email         string `json:"email"`
	City          string `json:"city"`
	State         string `json:"state"`
}

var esClient *elastic.Client

func InitEs() {
	host := "http://127.0.0.1:9200"
	logger := log.New(os.Stdout, "ygshop", log.LstdFlags)
	options := []elastic.ClientOptionFunc{
		elastic.SetURL(host),
		elastic.SetSniff(false),                          //是否开启集群嗅探
		elastic.SetHealthcheckInterval(10 * time.Second), //设置两次运行状况检查之间的间隔, 默认60s
		elastic.SetGzip(false),                           //启用或禁用gzip压缩
		elastic.SetTraceLog(logger),                      // 显示请求es的详细信息
		//elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)),  //ERROR日志输出配置
		//elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)),  //INFO级别日志输出配置
	}
	//options = append(options, elastic.SetBasicAuth(
	//	"xxxx",            //账号
	//	"xxxxxxxxxxxxxx",  //密码
	//))
	var err error
	esClient, err = elastic.NewClient(options...)
	if err != nil {
		panic(err)
	}
}

// 查询数据 bool查询（复合查询）
func TestBoolSearch(t *testing.T) {
	InitEs()
	boolQuery := elastic.NewBoolQuery()
	var mustQuery []elastic.Query
	mustQuery = append(mustQuery, elastic.NewMatchQuery("name", "果"))
	//mustQuery=append(mustQuery,elastic.NewTermQuery("is_hot", false))
	//mustQuery=append(mustQuery,elastic.NewTermQuery("is_new", true))
	//mustQuery=append(mustQuery,elastic.NewTermQuery("brands_id", 20))
	//mustQuery=append(mustQuery,elastic.NewRangeQuery("shop_price").Gte(10.0).Lte(50.0))
	////where category_id in (24, 25)
	mustQuery = append(mustQuery, elastic.NewTermsQuery("category_id", 24, 25))
	boolQuery.Must(mustQuery...)
	rsp, err := esClient.Search().
		Index("ygshop_goods").
		//偏移量
		From(0).
		//返回数据的条数
		Size(20).
		Query(boolQuery).
		Do(context.Background())
	if err != nil {
		t.Fatal("es查询失败：", err)
	}
	//var account Account
	t.Log("rsp.Hits.TotalHits.Value:", rsp.Hits.TotalHits.Value)

	for _, val := range rsp.Hits.Hits {
		good := new(model.EsGoods)
		json.Unmarshal(val.Source, good)
		t.Logf("%#v", good)
	}

	// 查看查询格式
	// {"bool":{"must":{"match":{"address":{"query":"street"}}}}}

	//src, _ := boolQuery.Source()
	//data, _ := json.Marshal(src)
	//t.Log(string(data))
}

// 查询数据 match查询
func TestMatchSearch(t *testing.T) {
	InitEs()
	t.Log("esClient==nil:", esClient == nil)
	q := elastic.NewMatchQuery("address", "street")
	rsp, err := esClient.Search().
		Index("accounts").
		//偏移量
		//From(0).
		//返回数据的条数
		Size(20).
		Query(q).
		Do(context.Background())
	if err != nil {
		t.Fatal("es查询失败：", err)
	}
	//var account Account
	t.Log("rsp.Hits.TotalHits.Value:", rsp.Hits.TotalHits.Value)

	for _, val := range rsp.Hits.Hits {
		account := new(Account)
		json.Unmarshal(val.Source, account)
		t.Logf("%#v", account)
	}

	// 查看查询格式
	// {"bool":{"must":{"match":{"address":{"query":"street"}}}}}
	src, _ := q.Source()
	data, _ := json.Marshal(src)
	t.Log(string(data))
}

// 添加数据
/*
POST my_user/_doc
{
  "account_number": 98237234,
  "balance": 0,
  "firstname": "mike",
  "lastname": "",
  "age": 0,
  "gender": 0,
  "address": "",
  "employer": "",
  "email": "",
  "city": "",
  "state": ""
}
*/
func TestInsertData(t *testing.T) {
	InitEs()
	account := &Account{
		AccountNumber: 98237234,
		Firstname:     "mike",
	}
	_, err := esClient.Index().
		Index("my_user").
		BodyJson(account).
		Do(context.Background())
	if err != nil {
		panic(err)
	}
	t.Log("添加成功")
}

const goodsMapping = `
{
	"mappings":{
		"properties":{
			"name":{
				"type":"text",
				"analyzer":"ik_max_word"
			},
			"id":{
				"type":"integer"
			}
		}
	}
}`

// 创建索引并配置mapping
func TestCreateIndexAndSetMapping(t *testing.T) {
	InitEs()
	_, err := esClient.
		CreateIndex("ygshop_goods").
		BodyString(goodsMapping).
		Do(context.Background())
	if err != nil {
		panic(err)
	}
	t.Log("创建索引并配置mapping成功")
}
