package utils

import "testing"

func TestGetAvailablePort(t *testing.T) {
	got, err := GetAvailablePort()
	if err != nil {
		t.Error(err)
	} else {
		t.Log(got)
	}
}
