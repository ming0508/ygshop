package initialize

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"log"
	"os"
	"service/order_srv/global"
	"time"
)

func InitDB() {
	mysqlCfg := global.ServerConfig.MysqlInfo
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", mysqlCfg.User, mysqlCfg.Password, mysqlCfg.Host, mysqlCfg.Port, mysqlCfg.Name)

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags),
		logger.Config{
			SlowThreshold: time.Second, //慢sql阈值
			LogLevel:      logger.Info, // Log level
			Colorful:      true,        // 是否彩色打印
		},
	)

	// 全局模式
	var err error
	global.DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
		// 这里是为了让生成的表名末尾不会自动添加s
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},

		Logger: newLogger,
	})
	if err != nil {
		panic("连接数据库失败：" + err.Error())
	}
}
