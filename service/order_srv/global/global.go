package global

import (
	"gorm.io/gorm"
	"service/order_srv/config"
	"service/order_srv/proto"
)

var (
	// 全局句柄
	DB *gorm.DB
	// service层用户服务的配置信息
	ServerConfig = new(config.ServerConfig)
	// 配置中心nacos的连接信息
	NacosConfig = new(config.NacosConfig)

	// 连接grpc服务的句柄
	GoodsSrvClient     proto.GoodsClient
	InventorySrvClient proto.InventoryClient
)
