package rocketmq

import (
	"context"
	"fmt"
	"github.com/apache/rocketmq-client-go/v2"
	"github.com/apache/rocketmq-client-go/v2/consumer"
	"github.com/apache/rocketmq-client-go/v2/primitive"
	"github.com/apache/rocketmq-client-go/v2/producer"
	"testing"
	"time"
)

// 生产普通消息
/*
发送成功：
SendResult [sendStatus=0, msgIds=0A3A559F21DB000000005069dc780001, offsetMsgId=0A3A559F00002A9F00000000000001A4, queueOffset=0, messageQueue=MessageQueue [topic=go-topic-test-simple-message, brokerName=broker-a, queueId=1]]
*/
func TestProduceSimpleMessage(t *testing.T) {
	p, err := rocketmq.NewProducer(producer.WithNameServer([]string{"127.0.0.1:9876"}))
	if err != nil {
		panic("实例化producer失败")
	}
	defer func() {
		if err = p.Shutdown(); err != nil {
			panic("关闭producer失败")
		}
	}()

	if err = p.Start(); err != nil {
		panic("启动producer失败")
	}

	res, err := p.SendSync(context.Background(), primitive.NewMessage(
		// topic不存在则会自动创建
		"go-topic-test-simple-message",
		[]byte("go-message-test")))
	if err != nil {
		t.Log("发送失败：", err)
	} else {
		t.Log("发送成功：\n", res.String())
	}
}

/*
   rocketmq_test.go:64: ------------------
   rocketmq_test.go:65: 获取到值： [Message=[topic=go-topic-test-simple-message, body=go-message-test, Flag=0, properties=map[CLUSTER:DefaultCluster CONSUME_START_TIME:1637047300003 MAX_OFFSET:2 MIN_OFFSET:0 UNIQ_KEY:0A3A559F21DB000000005069dc780001], TransactionId=], MsgId=0A3A559F21DB000000005069dc780001, OffsetMsgId=0A3A559F00002A9F00000000000001A4,QueueId=1, StoreSize=199, QueueOffset=0, SysFlag=0, BornTimestamp=1637045115412, BornHost=172.19.0.1:58850, StoreTimestamp=1637045115456, StoreHost=10.58.85.159:10911, CommitLogOffset=420, BodyCRC=702204255, ReconsumeTimes=0, PreparedTransactionOffset=0]
   rocketmq_test.go:64: ------------------
   rocketmq_test.go:65: 获取到值： [Message=[topic=go-topic-test-simple-message, body=go-message-test, Flag=0, properties=map[CLUSTER:DefaultCluster CONSUME_START_TIME:1637047300003 MAX_OFFSET:2 MIN_OFFSET:0 UNIQ_KEY:0A3A559F2359000000005076dc980001], TransactionId=], MsgId=0A3A559F2359000000005076dc980001, OffsetMsgId=0A3A559F00002A9F000000000000026B,QueueId=1, StoreSize=199, QueueOffset=1, SysFlag=0, BornTimestamp=1637045967528, BornHost=172.19.0.1:59150, StoreTimestamp=1637045967561, StoreHost=10.58.85.159:10911, CommitLogOffset=619, BodyCRC=702204255, ReconsumeTimes=0, PreparedTransactionOffset=0]
*/
// 消费普通消息
func TestConsumeSimpleMessage(t *testing.T) {
	c, err := rocketmq.NewPushConsumer(
		consumer.WithNameServer([]string{"127.0.0.1:9876"}),
		consumer.WithGroupName("ygshop"),
	)
	if err != nil {
		panic("实例化consumer失败")
	}
	defer func() {
		if err = c.Shutdown(); err != nil {
			panic("关闭consumer失败")
		}
	}()

	// 订阅消息
	if err = c.Subscribe(
		"go-topic-test-simple-message",
		consumer.MessageSelector{},
		func(ctx context.Context, msgs ...*primitive.MessageExt) (consumer.ConsumeResult, error) {
			for _, msg := range msgs {
				t.Log("------------------")
				t.Log("获取到值：", msg)
			}
			// 若使用consumer.ConsumeSuccess则读取成功后消息丢弃
			return consumer.ConsumeSuccess, nil
		},
	); err != nil {
		t.Log("读取消息失败")
	}

	if err = c.Start(); err != nil {
		panic("启动consumer失败")
	}

	// 防止主goroutine退出
	time.Sleep(time.Hour)
}

// 发送延时消息
func TestProduceDelyMessage(t *testing.T) {
	p, err := rocketmq.NewProducer(producer.WithNameServer([]string{"127.0.0.1:9876"}))
	if err != nil {
		panic("实例化producer失败")
	}
	defer func() {
		if err = p.Shutdown(); err != nil {
			panic("关闭producer失败")
		}
	}()

	if err = p.Start(); err != nil {
		panic("启动producer失败")
	}

	msg := primitive.NewMessage(
		// topic不存在则会自动创建
		"go-topic-test-simple-message",
		[]byte("[go-message-test] delay message"))
	// 延时10s
	msg.WithDelayTimeLevel(3)
	res, err := p.SendSync(context.Background(), msg)
	if err != nil {
		t.Log("发送失败：", err)
	} else {
		t.Log("发送成功：\n", res.String())
	}
}

// 发送事务消息
func TestProduceTransactionMessage(t *testing.T) {
	p, err := rocketmq.NewTransactionProducer(
		&OrderListener{},
		producer.WithNameServer([]string{"127.0.0.1:9876"}))
	if err != nil {
		panic("实例化producer失败")
	}
	defer func() {
		if err = p.Shutdown(); err != nil {
			panic("关闭producer失败")
		}
	}()

	if err = p.Start(); err != nil {
		panic("启动producer失败")
	}

	res, err := p.SendMessageInTransaction(context.Background(), primitive.NewMessage(
		// topic不存在则会自动创建
		"go-topic-test-trans-message",
		[]byte("[go-message-test] trans message02")))
	if err != nil {
		t.Log("发送失败：", err)
	} else {
		t.Log("发送成功：", res.String())
	}
	// 验证回查机制时需要开启
	time.Sleep(time.Hour)
}

type OrderListener struct{}

// 本地事务
func (o *OrderListener) ExecuteLocalTransaction(msg *primitive.Message) primitive.LocalTransactionState {
	fmt.Println("开始执行本地逻辑")
	time.Sleep(time.Second * 3)
	fmt.Println("执行本地逻辑成功")

	// 执行到这里如果出现无缘无故的失败情况，代码异常或者宕机，那么rmq的回查机制就会发挥作用
	return primitive.UnknowState

	// 半消息提交 这里无论返回commit还是rollback，rmq都不会执行回查的方法
	//return primitive.RollbackMessageState
}

// rmq回查
func (o *OrderListener) CheckLocalTransaction(msg *primitive.MessageExt) primitive.LocalTransactionState {
	fmt.Println("正在执行rmq的消息回查")
	time.Sleep(time.Second * 10)
	fmt.Println("rmq的消息回查执行完毕")

	// 如果回查时出现了宕机，那么服务下次启动时，rmq依然会发来回查请求
	return primitive.CommitMessageState
	//return primitive.RollbackMessageState
}
