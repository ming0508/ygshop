package handler

import (
	"context"
	"service/order_srv/global"
	"service/order_srv/initialize"
	pt "service/order_srv/proto"
	"testing"
)

func Init() {
	initialize.InitLogger()
	initialize.InnitConfig()
	initialize.InitDB()
}

func TestOrderServer_CartItemList(t *testing.T) {
	Init()
	us := new(OrderServer)
	r := &pt.UserInfo{Id: 2143136}
	res, err := us.CartItemList(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("res.Total:", res.Total)
	for _, v := range res.Data {
		t.Log("--------------------")
		t.Log(v.Id)
		t.Log(v.UserId)
		t.Log(v.GoodsId)
		t.Log(v.Nums)
		t.Log(v.Checked)
	}
}

func TestOrderServer_CreateCartItem(t *testing.T) {
	Init()
	us := new(OrderServer)
	r := &pt.CartItemRequest{
		UserId:  2143136999,
		GoodsId: 59,
		Nums:    3,
	}
	res, err := us.CreateCartItem(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%#v", res)
}

func TestOrderServer_UpdateCartItem(t *testing.T) {
	Init()
	us := new(OrderServer)
	r := &pt.CartItemRequest{
		UserId:  2143136999,
		GoodsId: 59,
		Nums:    666,
		Checked: true,
	}
	_, err := us.UpdateCartItem(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("更新成功")
}

func TestOrderServer_DeleteCartItem(t *testing.T) {
	Init()
	us := new(OrderServer)
	r := &pt.CartItemRequest{Id: 7}
	_, err := us.DeleteCartItem(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("删除成功")
}

func TestOrderServer_CreateOrder(t *testing.T) {
	Init()
	// 商品服务
	if conn := initialize.InitSrvConn(global.ServerConfig.GoodsSrvInfo.Name); conn != nil {
		global.GoodsSrvClient = pt.NewGoodsClient(conn)
	}
	// 库存服务
	if conn := initialize.InitSrvConn(global.ServerConfig.InventorySrvInfo.Name); conn != nil {
		global.InventorySrvClient = pt.NewInventoryClient(conn)
	}

	us := new(OrderServer)
	//r := &pt.OrderRequest{
	//	UserId:  2143136,
	//	Address: "北京海淀",
	//	Name:    "王先生",
	//	Mobile:  "13888888888",
	//	Post:    "速速发货",
	//}
	r := &pt.OrderRequest{
		UserId:  2143131232,
		Address: "北京昌平",
		Name:    "周杰伦",
		Mobile:  "13666666666",
		Post:    "好期待呀！",
	}
	res, err := us.CreateOrder(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%#v", res)
}

func Test_GenerateOrder(t *testing.T) {
	for i := 0; i < 10; i++ {
		orderSn := GenerateOrder()
		t.Log(orderSn)
		t.Log(len(orderSn))
	}
}

func TestOrderServer_UpdateOrderStatus(t *testing.T) {
	Init()
	us := new(OrderServer)
	r := &pt.OrderStatus{
		OrderSn: "202111031530270358539750001",
		Status:  "PAYING",
	}
	_, err := us.UpdateOrderStatus(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("更新成功")
}

func TestOrderServer_OrderList(t *testing.T) {
	Init()
	us := new(OrderServer)
	r := &pt.OrderFilterRequest{
		UserId:      2143136,
		Pages:       2,
		PagePerNums: 1,
	}
	res, err := us.OrderList(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("res.Total:", res.Total)
	for _, v := range res.Data {
		t.Logf("%#v\n", v)
	}
}

func TestOrderServer_OrderDetail(t *testing.T) {
	Init()
	us := new(OrderServer)
	r := &pt.OrderRequest{
		Id: 5,
	}
	res, err := us.OrderDetail(context.Background(), r)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%#v\n", res.OrderInfo)
	t.Log("------------")
	for _, v := range res.Goods {
		t.Logf("%#v\n", v)
	}
}
